# Usage
Create a USB image from an ISO

    ./create_usb_image_from_iso.py -d ~/images/ ~/images/tails-amd64-3.6.iso

Add some empty space for the persistent partition

    dd if=/dev/zero of=~/images/tails-amd64-3.6.img bs=1M count=2048 oflag=append conv=notrunc

The image can be booted via libvirt. Follow the [instructions for running Tails from a virtual USB  storage](https://tails.boum.org/doc/advanced_topics/virtualization/virt-manager/#index5h1) to add the USB image to a VM in virt-manager.

After booting the image, move the GPT backup header to the end of the image

    sudo sgdisk -e /dev/sda

